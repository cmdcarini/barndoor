# ARG MAGNET
FROM alpine:latest
ARG MAGNET
COPY scan.sh .
RUN apk add --no-cache clamav clamav-libunrar transmission-cli transmission-daemon bind-tools
RUN echo $MAGNET && \
	echo "{ \"utp-enabled\": \"false\", \"idle-seeding-limit\": 0, \"idle-seeding-limit-enabled\": \"true\", \"ratio-limit-enabled\": \"true\", \"ratio-limit\": 0 }" >> ~/settings.json && \
	mkdir ~/Downloads && \
	chmod +x scan.sh && \
	transmission-cli -w ~/Downloads -g ~/. -D $MAGNET && \
	transmission-cli -w ~/Downloads -g ~/. -D $MAGNET && \
	./scan.sh

