#!/bin/sh

docker build -t $1 --build-arg MAGNET=$2 .
docker cp $(docker create $1):/root/Downloads ~/Downloads/.
